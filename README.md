# libsmq

This is a simple message queue modeled after Go's channels. It is released
under an ISC license. It will run on the BSDs and Linux, although it will
not run on OS X (due to the fact that OS X does not support unnamed POSIX
semaphores).

Dependencies:
        * texinfo (for the manual)
        * CUnit (for the unit tests)

